import React, { Component } from 'react';
import { FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap';
import Switch from '@material-ui/core/Switch';

class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userInfo : this.props.profile,
            editing : false,
            error : false
        }
    }
    
    componentDidMount() {
        if(typeof this.props.profile.name === 'undefined') {
            this.props.fetchProfile();
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ userInfo: nextProps.profile })
    }

    handleChange(type, event) {
        var userInfoCopy = JSON.parse(JSON.stringify(this.state.userInfo));
        userInfoCopy[type] = event.target.value;
        this.setState({
            userInfo : userInfoCopy
        });
    }

    saveProfile() {
        var error = false;
        var propertiesToCheck = ['name', 'login', 'location'];
        propertiesToCheck.forEach((term) => {
            if(this.state.userInfo[term] === '') {
                error = true;
            }
        });
        if(!error) {
            this.props.saveProfile(this.state.userInfo);
        }
        this.setState({error : error});
    }

    render() {
        return (
            <div>
                <Switch
                  checked={this.state.editing}
                  color="primary"
                  onChange={() => { this.setState({editing: !this.state.editing})}}
                />
                {this.state.editing ? 
                    <FormGroup controlId="formBasicText">
                        
                        <ControlLabel>Name</ControlLabel>
                        <FormControl 
                        type="text" 
                        value={this.state.userInfo.name} 
                        placeholder="Enter text" 
                        className = {this.state.error && this.state.userInfo.name==='' ? 'red-border' : ''}
                        onChange={this.handleChange.bind(this, 'name')} />
                        
                        <ControlLabel>Login</ControlLabel>
                        <FormControl 
                        type="text" 
                        value={this.state.userInfo.login} 
                        placeholder="Enter text" 
                        className = {this.state.error && this.state.userInfo.login==='' ? 'red-border' : ''}
                        onChange={this.handleChange.bind(this, 'login')} />
                        
                        <ControlLabel>Location</ControlLabel>
                        <FormControl 
                        type="text" 
                        value={this.state.userInfo.location} 
                        placeholder="Enter text" 
                        className = {this.state.error && this.state.userInfo.location==='' ? 'red-border' : ''}
                        onChange={this.handleChange.bind(this, 'location')} />
                        
                        <br />
                        <Button bsStyle="success" onClick={this.saveProfile.bind(this)}>Save</Button>
                    </FormGroup> 
                :
                    <div>
                        <p><strong>Name:</strong> { this.state.userInfo.name } </p>
                        <p><strong>Login:</strong> { this.state.userInfo.login } </p>
                        <p><strong>Location:</strong> { this.state.userInfo.location } </p>
                    </div>
                }
                
            </div>
            );
    }
}

export default Profile;